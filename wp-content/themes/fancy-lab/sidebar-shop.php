<?php
/**
 * The template for sidebar containing the WooCommerce Shop widget area
 *
 * @package Fancy Lab
 */
?>

<?php if(is_active_sidebar( 'fancy-lab-sidebar-shop' )): ?>
        <?php dynamic_sidebar( 'fancy-lab-sidebar-shop' ) ?>		
<?php endif; 