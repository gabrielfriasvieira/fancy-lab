��    D      <  a   \      �     �  
   �     �     �          (     ?  
   V     a  	   v     �     �  !   �     �     �     �  +        7     W     m     �     �     �     �  $   �     �            
        %     2     K     c     v     �     �     �     �  +   �     �     �     	     2	     C	     L	     b	     o	     �	     �	     �	     �	     �	     �	     �	     �	      
  #    
     D
     V
     e
     t
  :   �
  U   �
  .        C     F     b  �  w     F     L     ]     s      �      �      �  
   �      �  	        '     <  *   Y     �  %   �     �  0   �  !        '     A  $   ]     �     �     �  5   �     	     !     5     :     F  ,   Y     �  %   �     �     �     �            6   "  2   Y  %   �  (   �     �     �     �               5     Q     m     �     �     �     �     �  '   �          2     F     [     p  ;   �  R   �  %        :     =     O             .              <   9   5       >       B   ,            *       "   '   8   $              
             @       :   =       1   2   3   6       %   !             -             A       &               ;   4      /   D   (      ?                          	                           C      )   +                 #       7                     0       % OFF Add a menu Add to Cart Blog Section Title Button Text for page 1 Button Text for page 2 Button Text for page 3 Categories Comments are closed. Copyright Copyright Section Copyright Settings Copyright X - All Rights Reserved Deal of the Week Deal of the Week Product ID Deal of the Week Title Drag and drop your WooCommerce widgets here Drag and drop your widgets here Fancy Lab Footer Menu Fancy Lab Main Menu Fancy Lab Main Sidebar Footer Sidebar 1 Footer Sidebar 2 Footer Sidebar 3 Home Page Products and Blog Settings Home Page Section Login / Register Logout My Account New Arrivals New Arrivals Max Columns New Arrivals Max Number New Arrivals Title News From Our Blog Next Nothing to display Page not found Pages Please, add your copyright information here Popular Products Max Columns Popular Products Max Number Popular Products Title Popular products Previous Product ID to display Published by Search results for Set slider page 1 Set slider page 2 Set slider page 3 Show deal of the week? Sidebar Shop Slider Section Slider Settings Tags Take a Look at Our Latest Posts There are no results for your query Toggle navigation URL for page 1 URL for page 2 URL for page 3 Unfortunately, the page you tried to reach does not exist. comments title%1$s thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; comments titleOne thought on &ldquo;%s&rdquo; on placeholderSearch &hellip; submit buttonSearch Project-Id-Version: Blank WordPress Pot v1.0.0
PO-Revision-Date: 
Language-Team: Your Team <translations@example.com>
Report-Msgid-Bugs-To: Translator Name <translations@example.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Textdomain-Support: yesX-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;esc_html_e;esc_html_x:1,2c;esc_html__;esc_attr_e;esc_attr_x:1,2c;esc_attr__;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_x:1,2c;_n:1,2;_n_noop:1,2;__ngettext:1,2;__ngettext_noop:1,2;_c,_nc:4c,1,2
X-Poedit-Basepath: ..
X-Generator: Poedit 3.0
Last-Translator: 
Language: pt_BR
X-Poedit-SearchPath-0: .
 % OFF Adicione um menu Adicionar ao carrinho Título da Seção do Blog Texto do botão para a página 1 Texto do botão para a página 2 Texto do botão para a página 3 Categorias Os comentários estão fechados. Copyright Seção de copyright Configurações de Copyright Copyright X - Todos os direitos reservados Promoção da semana ID do produto da promoção da semana Promoção da semana Arraste e solte seus widgets do WooCommerce aqui Arraste e solte seus widgets aqui Menu rodapé do Fancy Lab Menu principal do Fancy Lab Barra Lateral Principal do Fancy Lab Rodapé da Barra Lateral 1 Rodapé da Barra Lateral 2 Rodapé da Barra Lateral 3 Configurações de Produtos e de Blog da Página Home Seção da Página Home Login / Registre-se Sair Minha conta Novos Lançamentos Número máximo de colunas para lançamentos Número máximo de lançamentos Título da Seção Novos Lançamentos Notícias do Blog Próximo Não há nada para exibir Página não encontrada Páginas Por favor, digite suas informações de copyright aqui Número Máximo de Colunas para Produtos Populares Número máximo de produtos Populares Título da Seção de Produtos Populares Produtos Populares Anterior ID do produto a exibir Publicado por Resultados de busca para Definir página 1 do slider Definir página 2 do slider Definir página 3 do slider Mostrar a promoção da semana? Sidebar da Loja Seção do Slider Configurações do Slider Tags De uma olhada nos nossos últimos posts Sem resultados para a sua busca Alterar navegação URL para a página 1 URL para a página 2 URL para a página 3 Infelizmente, página que você tentou acessar não existe. %1$s pensamento sobre &ldquo;%2$s&rdquo; %1$s pensamentos sobre &ldquo;%2$s&rdquo; Um comentário sobre &ldquo;%s&rdquo; em Pesquisa &hellip; Procurar 